from django.db import models
from django.http import HttpResponseRedirect
from modelcluster.fields import ParentalKey
from modelcluster.models import ClusterableModel
from wagtail import blocks
from wagtail.contrib.table_block.blocks import TableBlock
from wagtail.admin.panels import (
    FieldPanel,
    FieldRowPanel,
    InlinePanel,
    MultiFieldPanel,
)
from wagtail.contrib.settings.models import BaseSiteSetting, register_setting
from wagtail.models import Page, Orderable
from wagtail.fields import (
    StreamField,
    RichTextField,
)
from wagtail.contrib.forms.models import (
    AbstractEmailForm,
    AbstractFormField,
)
from wagtail.contrib.forms.panels import (
    FormSubmissionsPanel,
)


# Settings

@register_setting
class UsefullLinksSettings(ClusterableModel,
                           BaseSiteSetting):
    """Usefull links who are at the bottom of each page"""
    panels = [
        InlinePanel(
            'usefull_links',
            label='Liens utiles',
        )
    ]

    class Meta:
        verbose_name = 'Liens utiles, bas de page'
        verbose_name_plural = 'Liens utiles, bas de page'


class UsefullLink(Orderable):
    """A usefull link"""
    setting = ParentalKey(
        UsefullLinksSettings,
        on_delete=models.CASCADE,
        related_name='usefull_links',
    )
    page = models.ForeignKey(
        Page,
        on_delete=models.CASCADE,
        related_name='+',
    )

    panels = [
        FieldPanel('page'),
    ]


# Page models

class SubMenuPage(Page):
    """A page used only to create a submenu"""

    class Meta:
        verbose_name = 'Page de sous-menu'
        verbose_name_plural = 'Pages de sous-menu'

    def is_for_submenu(self):
        """Tell if this page is for a submenu"""
        return True


class WorkInProgressPage(Page):
    """A page to indicate a work in progress"""

    class Meta:
        verbose_name = 'Page de travail en progression'
        verbose_name_plural = 'Pages de travail en progression'


class RedirectPage(Page):
    """A page who redirect to an URL"""
    url = models.URLField(
        verbose_name='URL',
    )

    content_panels = Page.content_panels + [
        FieldPanel('url'),
    ]

    def serve(self, request):
        return HttpResponseRedirect(self.url)

    class Meta:
        verbose_name = 'Page de redirection'
        verbose_name_plural = 'Pages de travail en redirection'
        

class StreamPage(Page):
    """A simple page with a stream field"""
    body = StreamField(
        [
            (
                'text',
                blocks.RichTextBlock(
                    features=[
                        'h2', 'h3', 'bold', 'italic', 'link', 'document-link',
                        'ul', 'ol', 'image',
                    ]
                ),
            ),
            (
                'table',
                TableBlock(),
            ),
            (
                'html',
                blocks.RawHTMLBlock(
                    help_text="Attention à l'accessibilité.",
                )
            ),
            
        ],
        use_json_field=True,
    )

    content_panels = Page.content_panels + [
        FieldPanel('body'),
    ]


# Form models

class FormField(AbstractFormField):
    """A form field"""
    page = ParentalKey(
        'EmailFormPage',
        on_delete=models.CASCADE,
        related_name='form_fields',
    )


class EmailFormPage(AbstractEmailForm):
    """A form page that send notifications by email"""
    intro = RichTextField(
        blank=True,
        verbose_name='introduction',
    )
    thanks_text = RichTextField(
        blank=True,
        verbose_name='Message de remerciement',
        help_text='Affiché une fois le formulaire rempli et enregistré',
    )

    content_panels = AbstractEmailForm.content_panels + [
        FormSubmissionsPanel(),
        FieldPanel('intro'),
        InlinePanel(
            'form_fields',
            label='Champs du formulaire',
        ),
        FieldPanel('thanks_text'),
        MultiFieldPanel(
            [
                FieldRowPanel(
                    [
                        FieldPanel(
                            'from_address',
                            classname='col6',
                        ),
                        FieldPanel(
                            'to_address',
                            classname='col6',
                        ),
                    ],
                ),
                FieldPanel('subject'),
            ],
            'Email notification',
        ),
    ]

    class Meta:
        verbose_name = 'Formulaire avec notification par email'
        verbose_name_plural = 'Formulaires avec notification par email'
