from django import template
from django.conf import settings

register = template.Library()

@register.simple_tag
def get_setting(setting_name):
    """Return the value of a setting"""
    return getattr(settings, setting_name, "")
