from django.db import models
from modelcluster.fields import ParentalKey
from wagtail.admin.panels import FieldPanel, InlinePanel
from wagtail.models import Page, Orderable



class HomePage(Page):
    """The home page"""
    event_logo = models.ForeignKey(
        'wagtailimages.Image',
        on_delete=models.PROTECT,
        related_name='+',
        verbose_name="Logo de l'événement",
    )
    event_logo_description = models.CharField(
        max_length=255,
        verbose_name='Description du logo',
    )
    event_name = models.CharField(
        max_length=255,
        verbose_name="Nom de l'événement",
    )
    event_edition = models.CharField(
        max_length=255,
        verbose_name="Édition de l'événement",
    )
    event_begin = models.DateField(
        verbose_name="Date de début de l'événement",
    )
    event_end = models.DateField(
        verbose_name="Date de fin de l'événement",
    )

    content_panels = Page.content_panels + [
        FieldPanel('event_name'),
        FieldPanel('event_logo'),
        FieldPanel('event_logo_description'),
        FieldPanel('event_edition'),
        FieldPanel('event_begin'),
        FieldPanel('event_end'),
        InlinePanel(
            'event_description_pages',
            label="Pages principales décrivant l'événement",
        ),
    ]
    
    
class EventDescriptionPage(Orderable):
    """A link placed on home page"""
    home_page = ParentalKey(
        HomePage,
        on_delete=models.CASCADE,
        related_name='event_description_pages',
    )
    page = models.ForeignKey(
        Page,
        on_delete=models.CASCADE,
        related_name='+',
    )

    panels = [
        FieldPanel('page'),
    ]

    class Meta(Orderable.Meta):
        verbose_name = "Page décrivant l'événement"
        verbose_name_plural = "Pages décrivant l'événement"

