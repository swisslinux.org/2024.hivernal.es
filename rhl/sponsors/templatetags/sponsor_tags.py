from django import template

from sponsors.models import (
    SponsorType,
    Sponsor,
)

register = template.Library()


@register.simple_tag
def get_sponsor_list(group_by_type=False):
    """Return the list of sponsors, as a queryset

    Parameter:
    * group_by_type (str): If group sponsors by type

    Return:
    * If group_by_type=True: A dictionary, sponsor type as keys
                             and sponsors as values
    * If group_by_type=False: A queryset with all sponsors
    """
    
    if group_by_type:

        # If group by type, create an empty dict
        sponsor_list = {}

        # FOr each sponsor type, get a sponsors queryset limited to it
        for sponsor_type in SponsorType.objects.all():
            sponsor_list[sponsor_type] = Sponsor.objects.filter(
                sponsor_type=sponsor_type
            )
            
        return sponsor_list
    
    else:
        # Else return all sponsors
        return Sponsor.objects.all()
    

    
