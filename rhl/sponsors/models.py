from django.db import models

from wagtail.admin.panels import FieldPanel
from wagtail.fields import RichTextField
from wagtail.snippets.models import register_snippet


# Models

@register_snippet
class SponsorType(models.Model):
    order = models.IntegerField(
        default=0,
        verbose_name='Ordre pour le tri',
    )
    name = models.CharField(
        max_length=255,
        verbose_name='Nom',
    )
    logo = models.ForeignKey(
        'wagtailimages.Image',
        on_delete=models.PROTECT,
        related_name='+',
        null=True,
        blank=True,
        verbose_name='Logo',
    )
    logo_description = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name='Description du logo',
    )

    panels = [
        FieldPanel('order'),
        FieldPanel('name'),
        FieldPanel('logo'),
        FieldPanel('logo_description'),
    ]

    class Meta:
        ordering = ['order']
        verbose_name = 'type de sponsor'
        verbose_name_plural = 'types de sponsor'

    def __str__(self):
        return self.name


@register_snippet
class Sponsor(models.Model):
    """Sponsor on the event"""
    name = models.CharField(
        max_length=255,
        verbose_name='Nom',
    )
    sponsor_type = models.ForeignKey(
        SponsorType,
        on_delete=models.CASCADE,
        null=True,
        verbose_name='Type de sponsor',
    )
    description = RichTextField(
        features=[
            'h2',
            'h3',
            'bold',
            'italic',
            'ol',
            'ul',
        ],
        verbose_name='Description',
    )
    logo = models.ForeignKey(
        'wagtailimages.Image',
        on_delete=models.PROTECT,
        related_name='+',
        verbose_name='Logo',
    )
    logo_description = models.CharField(
        max_length=255,
        verbose_name='Description du logo',
    )
    website = models.URLField(
        verbose_name='Site web',
    )

    panels = [
        FieldPanel('name'),
        FieldPanel('sponsor_type'),
        FieldPanel('description'),
        FieldPanel('logo'),
        FieldPanel('logo_description'),
        FieldPanel('website'),
    ]

    class Meta:
        ordering = ['name']
        verbose_name = 'sponsor'
        verbose_name_plural = 'sponsors'

    def __str__(self):
        return self.name
