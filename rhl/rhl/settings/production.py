from .base import *

from .utils import (
    get_env,
    string_to_bool,
    string_to_list,
)

DEBUG = False

SECRET_KEY = get_env(
    'RHL_SECRET_KEY',
    '',
)

ALLOWED_HOSTS = string_to_list(
    get_env(
        'RHL_ALLOWED_HOSTS',
        '127.0.0.1, localhost',
    )
)

# Reverse proxy
HTTP_X_FORWARDED_PROTO = True
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

try:
    from .local import *
except ImportError:
    pass
