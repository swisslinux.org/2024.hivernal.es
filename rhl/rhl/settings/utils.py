"""Some utils for this website."""

import os

FALSE_VALUES = [
    'false',
    'no',
    'n',
    '0',
]


def get_env(key, default):
    """Get a environ variable."""
    return os.environ.get(
        key,
        default,
    )


def string_to_bool(value: str):
    """Convert a string to boolean.

    Any other value than this give a True:
      'False',
      'No',
      'n',
      '0'

    case incensitive
    """
    return value.lower() not in FALSE_VALUES


def string_to_list(value: str) -> list[str]:
    """Convert a string with comma to a list of string."""
    return [
        item.strip()
        for item in value.split(',')
    ]
