#+TITLE: Tasks of this project


* Deployment

* Code

** Before first release

*** TODO In content web page, provide a table of content

*** TODO Provide a search section

*** TODO Provide a sitemap

* Accessibility

Fix everything before first release

** DONE When zoom only text, main menu is out of its box
:LOGBOOK:
- State "DONE"       from "TODO"       [2023-09-15 ven 06:26]
:END:

If you zoom all the page, everything is ok.

But if you zoom only the text, the main menu is printed outside of the
header.

The header is blue, but not outside of itself. So if the text go
outside, it's white text on white background.

Is it because of Bootstrap who don't detect it need to collapse the
menu ? Because only the text is zoomed ?

Is it because it's an horizontal menu, so it cannot be cut in 2
line ?

Do I need to set something on CSS ?


*** Reason

- The menu use an <ul> with bootstrap ~.navbar-nav~ CSS class
- This class set ~flex-direction~ to ~row~ but not set ~flex-wrap~
- So, when I zoom only the text, the list is not wrapped to stay on
  its parent container
- And the list go outside its parent container, on the right side

  
*** Solution

- On my own CSS file: For the ~.navbar-nav~ class, I set ~flex-wrap~ to ~wrap~


** DONE Check if the way keyboard navigation is managed is ok
:LOGBOOK:
- State "DONE"       from "TODO"       [2023-09-18 lun 09:35]
:END:

- When you put focus on a link in header, or footer, with the
  keyboard: A white rounded outline is drawn
- But because the links have no padding around them, the outline is
  too close to the text
- So, I add some padding only when keyboard set focus on them
- This result as the link grown and move a little

Is it a good think to do ?

It make the link with keyboard focus more visible, which is better for
visibility. But it also cause a little movement. Can it be a problem
for some disabilities ?


** Solution

I still use ~outline~ to indicate a keyboard focus, which not give modification to element size.

But, I replaced ~padding~ on focus with ~outline-offset~.
