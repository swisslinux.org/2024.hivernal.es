#+TITLE: Deploy the website for development

In this document, you will see how to deploy this CMS for development.


* Dependencies

- [[https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-and-upgrading-ansible][Ansible]]
- [[https://podman.io/][Podman]]
- [[https://docs.ansible.com/ansible/latest/collections/containers/podman/index.html][Containers.podman Ansible collection]]

To install Ansible and Podman, you can install them with the package
manager of your GNU/Linux distribution. If not, you can found more
info on their respective documentation.

To install the Ansible collection ~Container.podman~, simply run this
command after installing Ansible:

#+begin_src sh :eval never
  ansible-galaxy collection install containers.podman
#+end_src


* Deploy

Run this command to deploy locally:

#+begin_src sh :eval never
  ansible-playbook dev_deploy.yml
#+end_src

Then, run this command to create the super user:

#+begin_src sh :eval never
  podman exec -it rhl24_app python manage.py createsuperuser
#+end_src


* Settings

All the settings are described here: [[file:configuration.org]]

Some default settings are different in the dev container image:
- ~RHL_MEDIA_ROOT~: Set to "/media"
- ~RHL_STATIC_ROOT~: Set to "/static"
  

* Remove the deployment

Run this command to remove the deployment:

#+begin_src sh :eval never
  ansible-playbook --tags down dev_deploy.yml
#+end_src


