# RHL 2024 website #

Here, you can found the source code for the website we used for the
2024 edition of RHL.


## Documentation ##

You can found all the documentation into the `doc/` directory.


## Website source code ##

The actual website source code are inside the `rhl/` directory.

## Software used ##

For this website, web use:
- [Django](https://www.djangoproject.com/)
- [Wagtail](https://wagtail.org/)


## Licences ##

- This website: AGPLv3
- Django: Three-clause BSD license, Python license
- Wagtail: Three-clause BSD license


## Author ##

Sébastien Gendre <seb@k-7.ch>
